package br.com.pratica.profissional.backend.ProjetoADS.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.pratica.profissional.backend.ProjetoADS.model.entities.ERole;
import br.com.pratica.profissional.backend.ProjetoADS.model.entities.ProfessionalServices;
import br.com.pratica.profissional.backend.ProjetoADS.model.entities.Role;
import br.com.pratica.profissional.backend.ProjetoADS.model.entities.User;
import br.com.pratica.profissional.backend.ProjetoADS.model.repositories.RoleRepository;
import br.com.pratica.profissional.backend.ProjetoADS.model.repositories.ServicesRepository;
import br.com.pratica.profissional.backend.ProjetoADS.model.repositories.UserRepository;
import br.com.pratica.profissional.backend.ProjetoADS.payload.request.LoginRequest;
import br.com.pratica.profissional.backend.ProjetoADS.payload.request.SignupRequest;
import br.com.pratica.profissional.backend.ProjetoADS.payload.response.JwtResponse;
import br.com.pratica.profissional.backend.ProjetoADS.response.Response;
import br.com.pratica.profissional.backend.ProjetoADS.security.jwt.JwtUtils;
import br.com.pratica.profissional.backend.ProjetoADS.security.services.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;
	
	@Autowired
	private ServicesRepository servicesRepository;

	UserController userControler;

	ProfessionalServicesController professionalServicesController;

	@Autowired
	JwtUtils jwtUtils;

	private static final String ERROR_ROLE_NOT_FOUND = "Error: Role is not found.";

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(
				new JwtResponse(jwt, userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(), roles));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {

		Response<User> response = new Response<>();

		try {
			if (Boolean.TRUE.equals(userRepository.existsByUsername(signUpRequest.getUsername()))) {
				response.getErrors().add("Error: Username is already taken!");
			}

			if (Boolean.TRUE.equals(userRepository.existsByEmail(signUpRequest.getEmail()))) {
				response.getErrors().add("Error: Email is already in use!");
			}

			User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(),
					encoder.encode(signUpRequest.getPassword()), signUpRequest.getNome(), signUpRequest.getSobrenome(),
					signUpRequest.getCpf(), signUpRequest.getRg(), signUpRequest.getTelefone(), signUpRequest.getCep(),
					signUpRequest.getRua(), signUpRequest.getNumero(), signUpRequest.getBairro(),
					signUpRequest.getCidade(), signUpRequest.getEstado());

			Set<String> strRoles = signUpRequest.getRole();
			Set<Role> roles = new HashSet<>();
			if (strRoles == null) {
				Role userRole = roleRepository.findByName(ERole.ROLE_USER)
						.orElseThrow(() -> new RuntimeException(ERROR_ROLE_NOT_FOUND));
				roles.add(userRole);
			} else {
				strRoles.forEach(role -> {
					switch (role) {
					case "admin":
						Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
								.orElseThrow(() -> new RuntimeException(ERROR_ROLE_NOT_FOUND));
						roles.add(adminRole);

						break;
					case "Profissional":
						Role modRole = roleRepository.findByName(ERole.ROLE_PROFESSIONAL)
								.orElseThrow(() -> new RuntimeException(ERROR_ROLE_NOT_FOUND));
						roles.add(modRole);

						break;
					default:
						Role userRole = roleRepository.findByName(ERole.ROLE_USER)
								.orElseThrow(() -> new RuntimeException(ERROR_ROLE_NOT_FOUND));
						roles.add(userRole);
					}
				});
			}

			user.setRoles(roles);
			userRepository.save(user);
			response.setData(user);

			LoginRequest dados = new LoginRequest();
			dados.setUsername(user.getUsername());
			dados.setPassword(user.getPassword());
			setServices(user);

		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok(response);
	}

	private void setServices(User user) {
		try {
			ProfessionalServices profServices = new ProfessionalServices(false, false, false, false, false, false, false, false, false,
					false);
			try
			{
				Set<User> profIds = new HashSet<>();
				profIds.add(user);
				profServices.setProfServices(profIds);
				servicesRepository.save(profServices);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}