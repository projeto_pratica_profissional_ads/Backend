package br.com.pratica.profissional.backend.ProjetoADS.model.entities;

public enum ERole {

	ROLE_USER,
    ROLE_PROFESSIONAL,
    ROLE_ADMIN
}
