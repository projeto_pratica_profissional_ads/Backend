package br.com.pratica.profissional.backend.ProjetoADS.model.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.pratica.profissional.backend.ProjetoADS.model.entities.ERole;
import br.com.pratica.profissional.backend.ProjetoADS.model.entities.Role;


@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	
	Optional<Role> findByName(ERole name);
}