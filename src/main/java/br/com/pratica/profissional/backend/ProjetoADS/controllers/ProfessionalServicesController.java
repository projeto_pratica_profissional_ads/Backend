package br.com.pratica.profissional.backend.ProjetoADS.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.pratica.profissional.backend.ProjetoADS.model.entities.ProfessionalServices;
import br.com.pratica.profissional.backend.ProjetoADS.model.entities.User;
import br.com.pratica.profissional.backend.ProjetoADS.model.repositories.ServicesRepository;
import br.com.pratica.profissional.backend.ProjetoADS.payload.request.LoginRequest;
import br.com.pratica.profissional.backend.ProjetoADS.response.Response;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/professional")
public class ProfessionalServicesController extends UserController {

	@Autowired
	private ServicesRepository servicesRepository;

	@GetMapping("/get")
	public ResponseEntity<Response<ProfessionalServices>> getProfessionalServices(
			@Valid @RequestBody LoginRequest loginRequest) {

		Response<ProfessionalServices> response = new Response<>();

		User user = findUserByUsername(loginRequest.getUsername());
		ProfessionalServices profServices;
		Long userId = user.getId();

		try {
			ProfessionalServices userService = checarListaServicesUsers(userId);
		
			profServices = userService;
		
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			return ResponseEntity.badRequest().body(response);
		}
		response.setData(profServices);
		return ResponseEntity.ok(response);
	}

	
	@PostMapping("/set")
	public ResponseEntity<Response<ProfessionalServices>> setProfessionalServices(@Valid @RequestBody ProfessionalServices profServices) {
		
		Response<ProfessionalServices> response = new Response<>();
		
		try {
				
			servicesRepository.save(profServices);
				
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			return ResponseEntity.badRequest().body(response);
		}
		
		response.setData(profServices);
		return ResponseEntity.ok(response);
	}
	 

	public List<ProfessionalServices> obterProfServices() {
		return servicesRepository.findAll();
	}

	public ProfessionalServices checarListaServicesUsers(Long userId) {
		Iterable<ProfessionalServices> ids = obterProfServices();

		for (ProfessionalServices id : ids) {
			if (id.getId() == userId) {
				return id;
			}
		}
		return null;
	}
	
}
