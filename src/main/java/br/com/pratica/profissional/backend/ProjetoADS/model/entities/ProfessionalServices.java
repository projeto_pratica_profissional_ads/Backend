package br.com.pratica.profissional.backend.ProjetoADS.model.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "professionalServices")
public class ProfessionalServices {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private boolean service1;
	private boolean service2;
	private boolean service3;
	private boolean service4;
	private boolean service5;
	private boolean service6;
	private boolean service7;
	private boolean service8;
	private boolean service9;
	private boolean service10;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "professional_services_users", 
			joinColumns = @JoinColumn(name = "user_id"), 
			inverseJoinColumns = @JoinColumn(name = "professional_id"))
	private Set<User> prof_services = new HashSet<>();

	public ProfessionalServices() {
	}

	public ProfessionalServices(boolean service1, boolean service2, boolean service3, boolean service4,
			boolean service5, boolean service6, boolean service7, boolean service8, boolean service9,
			boolean service10) {
		super();
		this.service1 = service1;
		this.service2 = service2;
		this.service3 = service3;
		this.service4 = service4;
		this.service5 = service5;
		this.service6 = service6;
		this.service7 = service7;
		this.service8 = service8;
		this.service9 = service9;
		this.service10 = service10;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isService1() {
		return service1;
	}

	public void setService1(boolean service1) {
		this.service1 = service1;
	}

	public boolean isService2() {
		return service2;
	}

	public void setService2(boolean service2) {
		this.service2 = service2;
	}

	public boolean isService3() {
		return service3;
	}

	public void setService3(boolean service3) {
		this.service3 = service3;
	}

	public boolean isService4() {
		return service4;
	}

	public void setService4(boolean service4) {
		this.service4 = service4;
	}

	public boolean isService5() {
		return service5;
	}

	public void setService5(boolean service5) {
		this.service5 = service5;
	}

	public boolean isService6() {
		return service6;
	}

	public void setService6(boolean service6) {
		this.service6 = service6;
	}

	public boolean isService7() {
		return service7;
	}

	public void setService7(boolean service7) {
		this.service7 = service7;
	}

	public boolean isService8() {
		return service8;
	}

	public void setService8(boolean service8) {
		this.service8 = service8;
	}

	public boolean isService9() {
		return service9;
	}

	public void setService9(boolean service9) {
		this.service9 = service9;
	}

	public boolean isService10() {
		return service10;
	}

	public void setService10(boolean service10) {
		this.service10 = service10;
	}

	public Set<User> getProfServices() {
		return prof_services;
	}

	public void setProfServices(Set<User> prof_services) {
		this.prof_services = prof_services;
	}

}
