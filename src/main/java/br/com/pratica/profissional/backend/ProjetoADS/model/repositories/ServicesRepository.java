package br.com.pratica.profissional.backend.ProjetoADS.model.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.pratica.profissional.backend.ProjetoADS.model.entities.ProfessionalServices;


@Repository
public interface ServicesRepository extends JpaRepository<ProfessionalServices, Long> {
	
	Optional<ProfessionalServices> findById(Long id);
}