package br.com.pratica.profissional.backend.ProjetoADS.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.pratica.profissional.backend.ProjetoADS.model.entities.User;
import br.com.pratica.profissional.backend.ProjetoADS.model.repositories.UserRepository;
import br.com.pratica.profissional.backend.ProjetoADS.payload.request.LoginRequest;
import br.com.pratica.profissional.backend.ProjetoADS.payload.request.SignupRequest;
import br.com.pratica.profissional.backend.ProjetoADS.response.Response;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = "/api/usuario")
public class UserController {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	PasswordEncoder encoder;
	
	@GetMapping
	public @ResponseBody Iterable<User> obterUsuario() {
		return userRepository.findAll();
	}

	@GetMapping("/busca/id")
	public @ResponseBody User obterUsuarioId(@RequestParam(name = "id") int id) {
		
		Iterable<User> usuarios = obterUsuario();
		
		for(User usuario: usuarios) {
			if(usuario.getId() == id) {
				return usuario;
			}
		}
		return null;
	}

	@PostMapping("/busca/username")
	public @ResponseBody User findUserByUsername(@RequestParam(name = "username") String username) {
		
		Iterable<User> usuarios = obterUsuario();
		for(User usuario: usuarios) {
			if(usuario.getUsername().equals(username)) {
				return usuario;
			}
		}
		return null;
	}
	
	@PutMapping("/alterar")
	public ResponseEntity<Response<User>> updateUser(@Valid @RequestBody SignupRequest signUpRequest) {
		Response<User> response = new Response<>();
		
		User user = findUserByUsername(signUpRequest.getUsername());
		
		if(encoder.matches(signUpRequest.getPassword(), user.getPassword())) {
			user.setUsername(signUpRequest.getUsername());
			user.setEmail(signUpRequest.getEmail());
			user.setNome(signUpRequest.getNome());
			user.setSobrenome(signUpRequest.getSobrenome());
			user.setCpf(signUpRequest.getCpf()); 
			user.setRg(signUpRequest.getRg());
			user.setTelefone(signUpRequest.getTelefone()); 
			user.setCep(signUpRequest.getCep());
			user.setRua(signUpRequest.getRua());
			user.setNumero(signUpRequest.getNumero()); 
			user.setBairro(signUpRequest.getBairro());
			user.setCidade(signUpRequest.getCidade());
			user.setEstado(signUpRequest.getEstado());
			
			userRepository.save(user);
			response.setData(user);
			response.getErrors().add("No errors");
			return ResponseEntity.ok(response);
		}else {
			return null;
		}	
	}
	
	@DeleteMapping("/excluir")
	public ResponseEntity<Response<User>> deleteUser(@Valid @RequestBody LoginRequest loginRequest) {
		
		Response<User> response = new Response<>();
		User user = findUserByUsername(loginRequest.getUsername());

		try {
			userRepository.delete(user);
			response.getErrors().add("User excluded without errors");
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			response.getErrors().add(e.getMessage());
			return ResponseEntity.badRequest().body(response);
		}
	}
}
